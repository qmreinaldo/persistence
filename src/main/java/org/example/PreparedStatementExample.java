package org.example;

import java.sql.*;
import java.time.LocalDate;

public class PreparedStatementExample {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/sample-db";
        try (Connection connection = DriverManager.getConnection(url, "postgres", "1234")) {
            connection(connection, "Turing");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    private static void connection(Connection connection, String surname) {
        String sql  = "SELECT * FROM patients WHERE surname = ?";
        try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

            preparedStatement.setString(1, surname);

            try (ResultSet resultSet = preparedStatement.executeQuery()){
                while (resultSet.next()) {
                    int idPatient = resultSet.getInt("idPatient");
                    String name = resultSet.getString("patientName");
                    String surnamePatient = resultSet.getString("surname");
                    LocalDate birthday = resultSet.getDate("birthday").toLocalDate();
                    System.out.println(idPatient + " " + name + " " + surnamePatient + " " + birthday);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
