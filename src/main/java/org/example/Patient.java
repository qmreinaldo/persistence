package org.example;

import java.time.LocalDate;

public class Patient {
    private int idPatient;
    private String name;
    private String surname;
    private LocalDate birthday;

    public int getIdPatient() {
        return idPatient;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public Patient(int idPatient, String name, String surname, LocalDate birthday) {
        this.idPatient = idPatient;
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }
}
