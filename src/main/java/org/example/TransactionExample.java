package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.logging.Logger;


public class TransactionExample {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/sample-db";
        String user = "postgres";
        String password = "1234";

        try (Connection connection = DriverManager.getConnection(url, user, password)){
            insertion(connection);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void insertion(Connection connection) throws SQLException {
        connection.setAutoCommit(false); // Beginning transaction
        Patient[] patients = new Patient[] {
                new Patient(10, "Alexander", "Grothendieck", LocalDate.parse("1920-06-12")),
                new Patient(11, "David", "Hilbert", LocalDate.parse("1850-01-20")),
                new Patient(12, "Haskell", "Curry", LocalDate.parse("1990-04-05"))};
        String sql = "INSERT INTO patients (idPatient, patientName, surname, birthday) " +
                "VALUES (?, ?, ?, ?)";
        try(PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
            for (Patient patient : patients) {
                preparedStatement.setInt(1, patient.getIdPatient());
                preparedStatement.setString(2, patient.getName());
                preparedStatement.setString(3, patient.getSurname());
                preparedStatement.setObject(4, patient.getBirthday());
                int res = preparedStatement.executeUpdate();
                System.out.println(res + " record modifications");
                connection.commit();        // transaction OK
            }
        } catch (SQLException e) {
            connection.rollback();      // Rollback transaction in case something go wrong
        }
        connection.setAutoCommit(true); // End transaction
    }
}
