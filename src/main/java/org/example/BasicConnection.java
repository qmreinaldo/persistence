package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BasicConnection {
    private Connection connection = null;

    BasicConnection() throws SQLException {
        try {
            connection();
        } finally {
            closeConnection();
        }
    }

    public void connection() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/example";
        String user = "postgres";
        String password = "1234";
        this.connection = DriverManager.getConnection(url, user, password);
        System.out.println("Successfully connection");
    }

    public void closeConnection() throws SQLException {
        if (this.connection != null)
            this.connection.close();
    }
    public static void main(String[] args) {
        try {
            new BasicConnection();
        } catch (SQLException sqlException) {
            sqlException.printStackTrace();
        }
    }
}
