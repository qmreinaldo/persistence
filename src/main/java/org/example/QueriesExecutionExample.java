package org.example;

import java.sql.*;
import java.time.LocalDate;

public class QueriesExecutionExample {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/sample-db";
        try (Connection connection = DriverManager.getConnection(url, "postgres", "1234");
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery("SELECT * FROM patients")) {
            while (resultSet.next()) {
                int idPatient = resultSet.getInt("idPatient");
                String patientName = resultSet.getString("patientName");
                String surname = resultSet.getString("surname");
                LocalDate birthday = resultSet.getDate("birthday").toLocalDate();
                System.out.println(idPatient + " " + patientName + " " + surname + " " + birthday);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
