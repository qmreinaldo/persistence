package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertUpdateDeleteExample {
    public static void main(String[] args) {
        String url = "jdbc:postgresql://localhost:5432/sample-db";
        String user = "postgres";
        String password = "1234";
        try (Connection connection = DriverManager.getConnection(url, user, password)) {
            String sqlInsertion = "INSERT INTO patients (idPatient, patientName, surname, birthday) " +
                    "VALUES (4, 'Kurt', 'Godel', '1990-07-23')";
            String sqlDeletion = "DELETE FROM patients WHERE idPatient = 4";
            String sqlUpdate = "UPDATE patients SET birthday = '1990-01-01' WHERE idPatient = 4";

            updateExecution(connection, sqlInsertion);
            updateExecution(connection, sqlUpdate);
            updateExecution(connection, sqlDeletion);

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private static void updateExecution(Connection connection, String sql) {
        try (Statement statement = connection.createStatement()) {
            int nOfRecordModifications = statement.executeUpdate(sql);
            System.out.println(nOfRecordModifications + " records modified.");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
