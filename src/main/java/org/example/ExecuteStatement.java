package org.example;

import java.sql.*;
import java.time.LocalDate;

public class ExecuteStatement {
    private Connection connection = null;
    ExecuteStatement() throws SQLException {
        try {
            connection();
            this.query();

        }
        finally {
            closeConnection();
        }
    }

    public void connection() throws SQLException {
        String url = "jdbc:postgresql://localhost:5432/example";
        String user = "postgres";
        String password = "1234";
        this.connection = DriverManager.getConnection(url, user, password);
        System.out.println("Successfully connection");
    }

    public void closeConnection() throws SQLException {
        if (this.connection != null)
            this.connection.close();
    }

    private void query() throws SQLException {
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM students");
        while (resultSet.next()) {
            int id = resultSet.getInt("id_student");
            String name = resultSet.getString("name_student");
            String surname = resultSet.getString("surname");
            LocalDate birthday = resultSet.getDate("birthday").toLocalDate();
            System.out.println("Student " + id + "\n\t " + name + " " + surname + " " + birthday);
        }
        resultSet.close();  // help garbage collector
        statement.close();  // help garbage collector
    }
    public static void main(String[] args) {
        try {
            new ExecuteStatement();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

